<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery>declare function local:default($RequestDoc as element() )  as element() 
{
 let $XML_Message := $RequestDoc/*/*
 return 
$XML_Message
}
;
declare variable $RequestDoc external; 
local:default($RequestDoc)</con:xquery>
</con:xqueryEntry>