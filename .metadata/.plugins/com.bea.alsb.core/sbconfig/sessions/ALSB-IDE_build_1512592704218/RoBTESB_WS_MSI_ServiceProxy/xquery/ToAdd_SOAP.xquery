<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery>declare function local:default($RequestDoc as element() )  as element() 
{
&lt;SOAP-ENV:Envelope xmlns:SOAP-ENV= "http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC= "http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi= "http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd= "http://www.w3.org/2001/XMLSchema">
&lt;SOAP-ENV:Body>
{
 let $XML_Message := $RequestDoc
 return 
$XML_Message
}
&lt;/SOAP-ENV:Body>
&lt;/SOAP-ENV:Envelope>
}
;
declare variable $RequestDoc external; 
local:default($RequestDoc)</con:xquery>
</con:xqueryEntry>