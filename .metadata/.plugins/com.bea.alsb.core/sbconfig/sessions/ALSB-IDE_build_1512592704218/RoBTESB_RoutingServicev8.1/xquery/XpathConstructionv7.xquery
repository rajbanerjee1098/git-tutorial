<?xml version="1.0" encoding="UTF-8"?>
<con:xqueryEntry xmlns:con="http://www.bea.com/wli/sb/resources/config">
    <con:xquery>xquery version "1.0" encoding "Cp1252";
(:: pragma  parameter="$InputBody" type="xs:anyType" ::)
(:: pragma  type="xs:anyType" ::)
(: Author RodricksPremKumar.A :)
(: Version 1.0 for PHASE 1A :)
(: Version 2.0 for PHASE 1B fn:contains-partof, fn:contains , fn:starts-with(modified in 1B) :)
(: Version 3.0 for PHASE 2 fn:local-name , fn:valueOfIndex , fn:allButOneNodesHasSameValue  16/09/2010:)
(: Version 4.0 for PHASE 1 fn:notMatches  PAL Migration fn:namespace-uri  17/01/2011:)
(: Version 5.0 for Phase 3 of PAL migration fn:normalize-space fn:upper-case 19/9/2011:)
(: Author BalajiShanmugasundaram :)
(: Version 6.0 for Phase 9 of PAL migration.Case Insensitivity for the XML Attribute values while forming the routing key. 03/07/2012:)



(: 

1: fn:allButOneNodesHasSameValue:
    Purpose:     This is to check whether all the siblings have the same value except the first sibling 
    Example :   fn:allButOneNodesHasSameValue(/SiebelMessage/ListOfOrderResponse/OrderResponse/ListOfOrderItemResponse/OrderItemResponse/Status,"Open")
    Description : This function will check whether all the status siblings  has the value Open except the first sibling.

2: fn:valueOfIndex:
    Purpose:     This is to get the value of the particular node , in the case if its parent node has more than one occurrence in the payload.
    Example :   fn:valueOfIndex(/SiebelMessage/ListOfOrderResponse/OrderResponse/ListOfOrderItemResponse/OrderItemResponse/Status,"2”)
    Description : This function will return the value of  2nd  node , as OrderItemResponse node has more than one occurrence in the payload.
    
    
3: to check the value in the any of the index of a repeating node 
    For example: to get the value from SubStatus tag , a child of OrderItemResponse( here this is the iterated node)  and compare it with the value ‘Closed’.
    fn:contains(Envelope/Body/SiebelMessage/ListOfOrderResponse/OrderResponse/ListOfOrderItemResponse/OrderItemResponse/SubStatus[1],'Closed')

4: fn:compare : 
    
    Purpose:     To check whether the data in the XML at the Argument xpath exactly Matches/Equals the Argument String.If True, returns the Argument String, else returns null
    Example : fn:compare(Envelope/Body/SiebelMessage/standardHeader/serviceAddressing/to/contextItemList/contextItem[@contextName='sProductCode'],'S0208337')
    
5: fn:notMatches : 
    
    Purpose:     Returns  "DumpRoute" if the data in the XML at the Argument xpath DOES NOT exactly Matches/Equals the Argument String,else returns null
    Example : fn:notMatches(Envelope/Body/SiebelMessage/standardHeader/serviceAddressing/to/contextItemList/contextItem[@contextName='sProductCode'],'S0208337')

6: fn:namespace-uri:

    Purpose:     Returns  namespace uri of the given xpath
    Example : fn:namespace-uri(Envelope/Body/activateNextAvailableReturn/standardHeader/e2e/E2EDATA)


7:fn:normalize-space 

    Purpose:     Trims the psaces between the words
    Example : fn:normalize-space(Envelope/Body/activateNextAvailableReturn/standardHeader/e2e/E2EDATA)

8: fn:upper-case 

    Purpose:  Changes the value ionto Uppercase   
     Example : fn:upper-case(Envelope/Body/activateNextAvailableReturn/standardHeader/e2e/E2EDATA)

 
::)

declare namespace xf = "http://tempuri.org/Core_ESB/generateRoutingKey/";

declare function xf:generateRoutingKey($inputBody as node()*,
    $fetchedXpath as element())
    as xs:string {
    
        let $generatedXml :=  
            &lt;Result>
            {
                for $i in $fetchedXpath/RESULT_SET return 
                
                &lt;xpathContent>
                {
                    if (fn:starts-with($i/XPATH,"fn:")) then
                        let $functionName :=   fn:substring-before($i/XPATH,"(") return
                        let $xpathExpression :=   fn:substring-after($i/XPATH,"(") return
                       
                        let $valueToBeChecked :=
                        if(fn:contains($xpathExpression,",")) then 
                                 fn:substring(fn:substring-after($xpathExpression,","),2,fn:string-length(fn:substring-after($xpathExpression,",")) -3) 
                        else() return        
                        let $xpathExpression := 
                         if(fn:contains($xpathExpression,",")) then 
                               fn:substring-before($xpathExpression,",") 
                               
                         else(fn:substring($xpathExpression,1,fn:string-length($xpathExpression) -1)) return                               
                        
                      
                        if(fn:matches($functionName,"fn:contains-partof")) then
                             let $tempVar := xf:evaluateDynamicXpath($inputBody,fn:tokenize($xpathExpression ,"/"),"") return
                             if (fn:contains($tempVar,$valueToBeChecked)) then
                                   (: returns valueToBeChecked, if fn:contains-partof is used :)
                                   $valueToBeChecked
                             else() 
                      else if(fn:matches($functionName,"fn:notMatches")) then
                      		
                      		
		            let $tempVar := xf:evaluateDynamicXpath($inputBody,fn:tokenize($xpathExpression ,"/"),"") return
		            if(fn:not(fn:empty($tempVar)))then 
		            let $returnValue :=
                                                                             
                                                                                    fn:concat(fn:distinct-values(for $value in fn:tokenize($valueToBeChecked, ',') return
					     if (fn:compare($tempVar,$value)!=0) then
					         1
                                                                                    else(0)))
 					    return
  					    let $result :=    
                                                                                                       if (xs:string($returnValue)= '1')then
     						       "DumpRoute"
     					                       else()
    					    return
  					   $result
                                                              						
 					else("DumpRoute")	
                      else if (fn:matches($functionName,"fn:contains")) then 
                           let $tempVar := xf:evaluateDynamicXpath($inputBody,fn:tokenize($xpathExpression ,"/"),"") return
                           if (fn:contains($tempVar,$valueToBeChecked)) then
                                (: tempVar returns the actual value instead of matching value, if fn:contains is used :)
                                 $tempVar 
                           else()

                      else if (fn:matches($functionName,"fn:compare")) then 
                           let $tempVar := xf:evaluateDynamicXpath($inputBody,fn:tokenize($xpathExpression ,"/"),"") return
                           if (fn:compare($tempVar,$valueToBeChecked)=0) then
                                (: tempVar returns the actual value instead of matching value, if fn:contains is used :)
                                 $valueToBeChecked
                           else()
                        else if(fn:matches($functionName,"fn:starts-with")) then
                             let $tempVar := xf:evaluateDynamicXpath($inputBody,fn:tokenize($xpathExpression ,"/"),"") return
                             if (fn:starts-with($tempVar,$valueToBeChecked)) then
                                   (: valueToBeChecked returns the matching value, if fn:starts-with is used :)
                                   $valueToBeChecked
                             else()
                        else if(fn:matches($functionName,"fn:local-name")) then
                             let $tempVar := xf:evaluateDynamicXpath($inputBody,fn:tokenize($xpathExpression ,"/"),"fn:local-name") return
                                   $tempVar

                        else if(fn:matches($functionName,"fn:valueOfIndex")) then
                             let $indexOfNode := xs:int($valueToBeChecked)
                             let $tempVar := xf:evaluateDynamicXpath($inputBody,fn:tokenize($xpathExpression ,"/"),"") return
                               if(fn:count($tempVar)>=$indexOfNode) then
                                   $tempVar[$indexOfNode]
                               else($indexOfNode)
                        else if(fn:matches($functionName,"fn:allButOneNodesHasSameValue")) then
                           (: returns sequence of string from all the siblings example is  {Complete ,Open,Open...} :)
                             let $tempVar := xf:evaluateDynamicXpath($inputBody,fn:tokenize($xpathExpression ,"/"),"") return
                             (: except first item , all the item :)
                             let $itemsFromSecondPosition:= subsequence($tempVar ,2) return
                             if(fn:count(fn:distinct-values($itemsFromSecondPosition))=1) then 
		     		if(fn:matches($itemsFromSecondPosition[1],$valueToBeChecked)) then
				 (: returns the argument as return value:)
		         	 $valueToBeChecked
		    		else()
                             else()
	       else if(fn:matches($functionName,"fn:namespace-uri")) then
                           (: returns sequence of string from all the siblings example is  {Complete ,Open,Open...} :)
                               let $tempVar := xf:evaluateDynamicXpath($inputBody,fn:tokenize($xpathExpression ,"/"),"fn:namespace-uri") return
                                   $tempVar                               
                        else()
 
                     else(
                              xf:evaluateDynamicXpath($inputBody,fn:tokenize($i/XPATH,"/"),"") 
                          )
                }
                &lt;/xpathContent>
            }           
            &lt;/Result>
            let $xpathSeq := 
                  for $xpathNode in $generatedXml/xpathContent return
                     if(fn:string-length($xpathNode)!=0) then
					  	    	fn:concat($xpathNode)
                     else()
	    return
	
	    let $resultantXpathSeq:=
	    let $positionOfDumpPlan:=fn:index-of($xpathSeq,"DumpRoute") return
            if ($positionOfDumpPlan > 0 ) then
	           fn:subsequence($xpathSeq,1, $positionOfDumpPlan)
             else(
	          $xpathSeq 
             )  
	     return
	     let $resultForXpaths := fn:string-join($resultantXpathSeq,"::") return 
	     let $serviceName := fn:distinct-values($fetchedXpath/ROUTING_KEY) return
			if (fn:string-length($serviceName)!=0) then
			  fn:concat($serviceName,"::",$resultForXpaths)
			else ($resultForXpaths)
            
};

declare function xf:evaluateDynamicXpath($inputBody as node()*,$xpathNodeArray as xs:string*,$functionName as xs:string)
as xs:string*{
	if(fn:empty($xpathNodeArray)) then
	    if ((fn:string-length($functionName)=0)) then
		fn:normalize-space(fn:lower-case($inputBody/text()))
	    else(

                       if(fn:matches($functionName,"fn:local-name"))then
                         data(fn:local-name($inputBody))
                       else if(fn:matches($functionName,"fn:namespace-uri")) then
                          xs:string(fn:namespace-uri($inputBody))
                       else()
                   )	
	else if($xpathNodeArray[1] = "") then 
		xf:evaluateDynamicXpath($inputBody/root(),subsequence($xpathNodeArray,2),$functionName)
	else if(contains($xpathNodeArray[1], "[@"))then 
		if(contains($xpathNodeArray[1],"=")) then
                                     let $subStringBefore:=fn:substring-before($xpathNodeArray[1],"[@") return
                                     let $subStringAfter:=fn:substring-after($xpathNodeArray[1],"[@") return
                                     let $predicate:=fn:substring-before($subStringAfter,"]") return 
                                     let $attrName := fn:substring(fn:tokenize($predicate,"=")[1],1) return
		     						let $attrValue := fn:substring(fn:tokenize($predicate,"=")[2],2,fn:string-length(fn:tokenize($predicate,"=")[2])-2) return
                                    for $nodeset in $inputBody/*[local-name() = $subStringBefore] 
                                    (:$nodeset will refer to the tag where attribute is present:)
                                 		let $matchValue := $nodeset/@*[local-name()=$attrName] return
                                 		(:$matchValue will have the value of the attribute.:)
                                    		if (lower-case($matchValue) = lower-case($attrValue)) then
                                    		(:compare XML attribute against the expected attribute from DB:)
                                        		fn:normalize-space(fn:lower-case(data(($nodeset))))
                                        		(:use the data of the tag where the attribute is present for forming the key:)
                                    		else()
          			else()	
                else(   
                                    
              if(starts-with($xpathNodeArray[1],"@")) then
                         let $attrName := fn:substring($xpathNodeArray[1],2) return
                               for $nodeset in $inputBody where $inputBody/@*[local-name()=$attrName] return 
                                        
										fn:normalize-space(fn:lower-case(data($nodeset/@*[local-name()=$attrName])))
                                          (: nodeset value returns the attribute value :)
                   
               else( 
                         let $inputBodyTemp := 
                            (: To get the value of particular index of an repeated node :) 
                            if (fn:contains($xpathNodeArray[1], "[")) then 
                                let $subStringBefore:=fn:substring-before($xpathNodeArray[1],"[") return
                                let $subStringAfter:=fn:substring-after($xpathNodeArray[1],"[") return
                                let $index:=fn:substring-before($subStringAfter,"]") return
                                 if(fn:matches($index,"[0-9]+")) then 
                                            $inputBody[xs:int($index)]/*[local-name() = $subStringBefore]
                                  else()
                                    else ($inputBody/*[local-name() = $xpathNodeArray[1]]) return

xf:evaluateDynamicXpath($inputBodyTemp,subsequence($xpathNodeArray,2),$functionName)
)
              )

};



declare variable $body as node()* external;
declare variable $fetchXPATH as element() external;



xf:generateRoutingKey($body,$fetchXPATH)</con:xquery>
</con:xqueryEntry>