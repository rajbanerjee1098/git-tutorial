declare variable $keyWord external;

declare function local:default($RequestDoc as element() , $keyWord)  as element() 
{
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC= "http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi= "http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd= "http://www.w3.org/2001/XMLSchema" xmlns:header= "http://wsi.nat.bt.com/2005/06/StandardHeader/">
<SOAP-ENV:Body>
<header:updateServiceRequest>
<header:standardHeader> 
		<header:e2e>
			<header:E2EDATA>
				{
					data(fn:concat("E2E.busProcOriginator=RoBTESB,E2E.busProcType=ManageServiceInventory,busProcID=",fn:concat($RequestDoc/*:TLVROOT/*:headers/*:SYSID/text(),"-",$RequestDoc/*:TLVROOT/*:headers/*:SYSINS/text(),"-",$RequestDoc/*:TLVROOT/*:headers/*:SYSREF/text(),"-",$RequestDoc/*:TLVROOT/*:headers/*:SYSRFQ/text(),"-",$RequestDoc/*:TLVROOT/*:headers/*:JOBTYP/text(),"-",$RequestDoc/*:TLVROOT/*:headers/*:EVENTT/text())))
				}
			</header:E2EDATA>
		</header:e2e>
		<header:serviceState>
			<header:stateCode>
				{
					data('OK')
				}
			</header:stateCode>
			<header:errorCode>
				{
					data('0')
				}
			</header:errorCode>
			<header:errorDesc>
				{
					data('Success')
				}
			</header:errorDesc>
		</header:serviceState>
		<header:serviceAddressing>
			<header:from>
				{
					data('20CNFlowStream')
				}
			</header:from>
			<header:to>
				<header:address>
					{
						data('http://fnetas.bt.com')
					}
				</header:address>
			</header:to>
			<header:messageId>
				{
					fn:concat($RequestDoc/*:TLVROOT/*:headers/*:SYSID/text(),"-",$RequestDoc/*:TLVROOT/*:headers/*:SYSINS/text(),"-",$RequestDoc/*:TLVROOT/*:headers/*:SYSREF/text(),"-",$RequestDoc/*:TLVROOT/*:headers/*:SYSRFQ/text(),"-",$RequestDoc/*:TLVROOT/*:headers/*:JOBTYP/text(),"-",$RequestDoc/*:TLVROOT/*:headers/*:EVENTT/text())
				}
			</header:messageId>
			<header:serviceName>
				{
					$RequestDoc/*:TLVROOT/*:payload/*:RequestType/text()
				}
			</header:serviceName>			
		</header:serviceAddressing>
</header:standardHeader>
</header:updateServiceRequest>
{
$RequestDoc/*:TLVROOT
}
</SOAP-ENV:Body></SOAP-ENV:Envelope>
}
;

declare variable $RequestDoc external; 
local:default($RequestDoc, $keyWord)