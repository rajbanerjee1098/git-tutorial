declare namespace mtos = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/MTOSI"; 
declare namespace agr = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/CCM/Agreements"; 
declare namespace xsl = "http://www.w3.org/1999/XSL/Transform"; 
declare namespace sup = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/CCM/Parties/PartyRoles/Supplier"; 
declare namespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/"; 
declare namespace non = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/NonCCM"; 
declare namespace res = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/CCM/Resources"; 
declare namespace stan = "http://wsi.nat.bt.com/2005/06/StandardHeader/"; 
declare namespace S7 = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20"; 
declare namespace ns = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20"; 
declare namespace cus = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/CCM/Parties/PartyRoles/Customer";
declare namespace S95 = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/CCM/Services"; 
declare namespace ser = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/CCM/Services"; 
declare namespace tns = "http://schemas.xmlsoap.org/soap/envelope/"; 
declare namespace S164 = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/NonCCM"; 
declare namespace headerlims = "http://wsi.nat.bt.com/2005/06/StandardHeader/"; 

 
declare function local:index-of-node 
  ( $nodes as node()* ,
    $nodeToFind as node() )  as xs:integer* {
       
  for $seq in (1 to count($nodes))
  return $seq[$nodes[$seq] is $nodeToFind]
 } ;


declare function local:default($RequestDoc as element() )  as element() 
 
{
<soapenv:Envelope xmlns:soapenv= "http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns= "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20" xmlns:stan= "http://wsi.nat.bt.com/2005/06/StandardHeader/" xmlns:ser= "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/CCM/Services" xmlns:res= "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/CCM/Resources" xmlns:non= "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/NonCCM" xmlns:mtos= "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/MTOSI" xmlns:sup= "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/CCM/Parties/PartyRoles/Supplier" xmlns:agr= "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/CCM/Agreements" xmlns:cus= "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/CCM/Parties/PartyRoles/Customer" xmlns:headerlims= "http://wsi.nat.bt.com/2005/06/StandardHeader/">
<soapenv:Body>
<ns:serviceNotification1>
{

 let $stan_standardHeader := $RequestDoc//ns:serviceNotification1/stan:standardHeader
 return 
$stan_standardHeader
 

}
<ns:serviceInstance>
<ser:serviceInstanceIdentifier xmlns:S164= "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/NonCCM" xmlns:tns= "http://schemas.xmlsoap.org/soap/envelope/" xmlns:S7= "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20">
<S95:value xmlns:S95= "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2007/04/20/CCM/Services">
{
data($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:serviceInstanceIdentifier/S95:value)

}
</S95:value>
</ser:serviceInstanceIdentifier>
<ser:status>
{
data($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:status)

}
</ser:status>
<ser:serviceRoot>
{
if ($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:serviceRoot/ser:supplierServiceRoot  ) then
for $ser_supplierServiceRoot in $RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:serviceRoot/ser:supplierServiceRoot
 return 
let $posz := local:index-of-node($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:serviceRoot/ser:supplierServiceRoot,$ser_supplierServiceRoot) return 

if ($posz < 3 +1  ) then
<ser:supplierServiceRoot>
<ser:serviceRootId>
{
data($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:serviceRoot/ser:supplierServiceRoot[position()=$posz]/ser:serviceRootId)

}
</ser:serviceRootId>
<ser:status>
{
data($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:serviceRoot/ser:supplierServiceRoot[position()=$posz]/ser:status)

}
</ser:status>
<ser:servicesSpecification>
<ser:id>
{
data($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:serviceRoot/ser:supplierServiceRoot[position()=$posz]/ser:servicesSpecification/ser:id)

}
</ser:id>
</ser:servicesSpecification>
</ser:supplierServiceRoot>

 else()


 else()
}
<ser:servicesSpecification>
<ser:id>
{
data($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:serviceRoot/ser:servicesSpecification/ser:id)

}
</ser:id>
</ser:servicesSpecification>
</ser:serviceRoot>
<ser:servicesSpecification>
<ser:id>
{
data($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:servicesSpecification/ser:id)

}
</ser:id>
</ser:servicesSpecification>
{
for $ser_serviceCharacteristic in $RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:serviceCharacteristic
 return 
let $firstpos := local:index-of-node($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:serviceCharacteristic,$ser_serviceCharacteristic) return
let $coun := count($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:serviceCharacteristic)
return
if ($firstpos<$coun +1  ) then
<ser:serviceCharacteristic>
<ser:name>
{
data($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:serviceCharacteristic[position()=$firstpos]/ser:name)

}
</ser:name>
<ser:serviceCharacteristicValue>
<ser:value>
{
data($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:serviceCharacteristic[position()=$firstpos]/ser:serviceCharacteristicValue/ser:value)

}
</ser:value>
</ser:serviceCharacteristicValue>
</ser:serviceCharacteristic>

 else()

}
{
if ($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:relatedServiceInstance  ) then

 let $ser_relatedServiceInstance := $RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:relatedServiceInstance
 return 
$ser_relatedServiceInstance
 


 else()
}
{
if ($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:serviceRoot/ser:accessPoint[2]/res:connectionTerminationPointHierarchy/non:connectionTerminationPoint/mtos:accessPointIdentifier/res:resourceSpecificationtype ) then

<ser:serviceCharacteristic>
					<ser:name>BRAS_TYPE</ser:name>
					<ser:serviceCharacteristicValue>
                    <ser:value>
                   { 
                    data($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:serviceRoot/ser:accessPoint[2]/res:connectionTerminationPointHierarchy/non:connectionTerminationPoint/mtos:accessPointIdentifier/res:resourceSpecificationtype )
                    }
                    </ser:value>
	            </ser:serviceCharacteristicValue>
	 </ser:serviceCharacteristic>

 else()
}

<ser:maintenanceCategory>
<agr:name>
{
data($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:maintenanceCategory/agr:name)

}
</agr:name>
</ser:maintenanceCategory>
<ser:customerAccount>
<cus:id>
{
data($RequestDoc//ns:serviceNotification1/ns:serviceInstance/ser:customerAccount/cus:id)

}
</cus:id>
</ser:customerAccount>
</ns:serviceInstance>
</ns:serviceNotification1>
</soapenv:Body>
</soapenv:Envelope>
}
;



declare variable $RequestDoc external; 
local:default($RequestDoc)