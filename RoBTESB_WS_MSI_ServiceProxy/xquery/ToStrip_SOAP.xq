declare function local:default($RequestDoc as element() )  as element() 
{
 let $XML_Message := $RequestDoc/*/*
 return 
$XML_Message
}
;
declare variable $RequestDoc external; 
local:default($RequestDoc)