declare namespace SOAP-ENV = "http://schemas.xmlsoap.org/soap/envelope/"; 
declare namespace xsl = "http://www.w3.org/1999/XSL/Transform"; 
declare namespace sh = "http://wsi.nat.bt.com/2005/06/StandardHeader/"; 
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance"; 
declare namespace p5 = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2008/04/16/CCM/Parties/PartyRoles/Supplier"; 
declare namespace xsd = "http://www.w3.org/2001/XMLSchema"; 
declare namespace p4 = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2008/04/16/CCM/Services"; 
declare namespace p3 = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2008/04/16/NonCCM"; 
declare namespace p2 = "http://wsi.nat.bt.com/2005/06/StandardHeader/"; 
declare namespace p1 = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2008/04/16"; 
declare namespace SOAP-ENC = "http://schemas.xmlsoap.org/soap/encoding/"; 


declare function local:concatMessageID($RequestDoc as element() , $SYSID as xs:string , $SYSINS as xs:string , $SYSREF as xs:string , $SYSRFQ as xs:string , $JOBTYP as xs:string , $EVENTT as xs:string , $delimiter as xs:string) 

{
data(concat($SYSID,$delimiter,$SYSINS,$delimiter,$SYSREF,$delimiter,$SYSRFQ,$delimiter,$JOBTYP,$delimiter,$EVENTT))

}
;

declare function local:default($RequestDoc as element() )  as element() 
 
{
<SOAP-ENV:Envelope xmlns:SOAP-ENV= "http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC= "http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi= "http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd= "http://www.w3.org/2001/XMLSchema" xmlns:sh= "http://wsi.nat.bt.com/2005/06/StandardHeader/">
<SOAP-ENV:Body>
<p1:updateServiceRequest xsi:schemaLocation= "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2008/04/16 file:///c:/Documents%20and%20Settings/nk0013489/Desktop/Async/ManageServiceInventoryProvider.20080416.xsd" xmlns:p1= "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2008/04/16" xmlns:p5= "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2008/04/16/CCM/Parties/PartyRoles/Supplier" xmlns:p4= "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2008/04/16/CCM/Services" xmlns:p3= "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2008/04/16/NonCCM" xmlns:p2= "http://wsi.nat.bt.com/2005/06/StandardHeader/">

<p2:standardHeader>
<p2:serviceAddressing>
<p2:from>20CNFlowStream</p2:from>
<p2:to>
<p2:address>http://capabilities.nat.bt.com/ManageServiceInventory/2008/04/16</p2:address>
</p2:to>
<p2:replyTo>
<p2:address>20CNFlowStream</p2:address>
<p2:contextItemList>
<p2:contextItem contextId= "FLOW" contextName= "ServiceType">Tie Cable Tie Pair MPF Service Id Mapping</p2:contextItem>
</p2:contextItemList>
</p2:replyTo>
<p2:messageId>
{
let $delimiter := '-' return
local:concatMessageID($RequestDoc,$RequestDoc//SYSID,$RequestDoc//SYSINS,$RequestDoc//SYSREF,$RequestDoc//SYSRFQ,$RequestDoc//JOBTYP,$RequestDoc//EVENTT,$delimiter)

}
</p2:messageId>
<p2:serviceName>http://capabilities.nat.bt.com/ManageServiceInventory</p2:serviceName>
<p2:action>http://capabilities.nat.bt.com/ManageServiceInventory#updateServiceRequest</p2:action>
</p2:serviceAddressing>
<p2:serviceSpecification>
<p2:payloadFormat>XML</p2:payloadFormat>
<p2:version>5.0</p2:version>
</p2:serviceSpecification>
</p2:standardHeader>
<p1:updateServiceRequest>
<p3:serviceInstanceIdentifier>
<p4:value>
{
data($RequestDoc//payload/MPFServiceId)

}
</p4:value>
</p3:serviceInstanceIdentifier>
<p3:updateServiceContent>
<p3:currentAttributeName>OrderType</p3:currentAttributeName>
<p3:newAttributeValue>
{
data($RequestDoc//payload/OrderType)

}
</p3:newAttributeValue>
</p3:updateServiceContent>
<p3:updateServiceContent>
<p3:currentAttributeName>Old TieCableId</p3:currentAttributeName>
<p3:newAttributeValue>
{
data($RequestDoc//payload/Old_TieCableId)

}
</p3:newAttributeValue>
</p3:updateServiceContent>
<p3:updateServiceContent>
<p3:currentAttributeName>Old TiepairID</p3:currentAttributeName>
<p3:newAttributeValue>
{
data($RequestDoc//payload/Old_TiepairID)

}
</p3:newAttributeValue>
</p3:updateServiceContent>
<p3:updateServiceContent>
<p3:currentAttributeName>New TieCableId</p3:currentAttributeName>
<p3:newAttributeValue>
{
data($RequestDoc//payload/New_TieCableId)

}
</p3:newAttributeValue>
</p3:updateServiceContent>
<p3:updateServiceContent>
<p3:currentAttributeName>New TiepairID</p3:currentAttributeName>
<p3:newAttributeValue>
{
data($RequestDoc//payload/New_TiepairID)

}
</p3:newAttributeValue>
</p3:updateServiceContent>
</p1:updateServiceRequest>
</p1:updateServiceRequest>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
}
;



declare variable $RequestDoc external; 
local:default($RequestDoc)