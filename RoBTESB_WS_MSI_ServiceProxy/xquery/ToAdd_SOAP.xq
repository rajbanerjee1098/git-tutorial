declare function local:default($RequestDoc as element() )  as element() 
{
<SOAP-ENV:Envelope xmlns:SOAP-ENV= "http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENC= "http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsi= "http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd= "http://www.w3.org/2001/XMLSchema">
<SOAP-ENV:Body>
{
 let $XML_Message := $RequestDoc
 return 
$XML_Message
}
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>
}
;
declare variable $RequestDoc external; 
local:default($RequestDoc)