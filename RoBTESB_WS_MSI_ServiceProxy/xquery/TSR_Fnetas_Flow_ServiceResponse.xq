declare namespace xsl = "http://www.w3.org/1999/XSL/Transform"; 
declare namespace p3 = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2008/04/16/CCM/Services"; 
declare namespace xsi = "http://www.w3.org/2001/XMLSchema-instance"; 
declare namespace p2 = "http://wsi.nat.bt.com/2005/06/StandardHeader/"; 
declare namespace p1 = "http://capabilities.nat.bt.com/xsd/ManageServiceInventory/2008/04/16"; 


declare function local:UnGroupMessageID($msg as xs:string,$elements as xs:string,$delimiter as xs:string) as element()* 
{

 if ($delimiter and fn:contains($msg,$delimiter)) then
 (
 local:UnGroupMessageID(fn:substring-after($msg,$delimiter),fn:substring-after($elements,$delimiter),$delimiter), 
 
 element{fn:substring-before($elements,$delimiter)}
 { 
 fn:substring-before($msg,$delimiter)
 }
 )
 
 else(
 element{$elements}{
 $msg
 }
 )
}
;



declare function local:default($RequestDoc as element() )  as element() 
 
{
<TLVROOT>
<headers>
{
let $msg := $RequestDoc/*//p2:relatesTo return
let $elements := "SYSID-SYSINS-SYSREF-SYSRFQ-JOBTYP-EVENTT" return
let $delimiter := "-" return
local:UnGroupMessageID($msg,$elements,$delimiter)

}
</headers>
<payload>
<ResponseType>
{
let $ResponseType := local-name($RequestDoc/*//p1:updateServiceResponse)
return
data($ResponseType)

}
</ResponseType>
{
if ($RequestDoc/*//p2:stateCode!=''  ) then
<stateCode>
{
data($RequestDoc/*//p2:stateCode)

}
</stateCode>

 else()
}
{
if ($RequestDoc/*//p2:errorCode!=''  ) then
<errorCode>
{
data($RequestDoc/*//p2:errorCode)

}
</errorCode>

 else()
}
{
if ($RequestDoc/*//p2:errorDesc!=''  ) then
<errorDesc>
{
data($RequestDoc/*//p2:errorDesc)

}
</errorDesc>

 else()
}
{
if ($RequestDoc/*//p2:errorText!=''  ) then
<errorText>
{
data($RequestDoc/*//p2:errorText)

}
</errorText>

 else()
}
<messageId>
{
data($RequestDoc/*//p2:messageId)

}
</messageId>
<MPFServiceId>
{
data($RequestDoc/*//p3:value)

}
</MPFServiceId>
</payload>
</TLVROOT>
}
;



declare variable $RequestDoc external; 
local:default($RequestDoc)